/* eslint-disable class-methods-use-this */
const fs = require('fs')
const _ = require('lodash')


class JSON_Generator {
  JSON_Generator() {
    this.generateJSON = this.generateJSON.bind(this)
  }

  generateJSON(json_faqs, output) {
    let optimized_qna_json = []
    let counter = 0
    json_faqs.forEach((faq) => {
      counter += 1

      optimized_qna_json.push({
        id: faq.original_question.replace(/[^\w\s]/gi, '').replace(/ /g, '_'),
        data: {
          questions: { en: faq.rephrased_questions, },
          answers: { en: [faq.answer], },
          redirectFlow: "",
          redirectNode: "",
          action: 'text',
          category: 'global',
          enabled: true
        }
      })

      if (counter === json_faqs.length) {
        fs.writeFileSync(`${__dirname}/results/botpress_ready/${output}.JSON`, JSON.stringify(optimized_qna_json.filter(qna => 'id' in qna && qna.id.length > 0)))
      }
    })
  }
}

module.exports = JSON_Generator
